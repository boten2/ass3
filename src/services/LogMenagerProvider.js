/**
 * Created by nadavv on 8/23/14.
 */
(function(){

    function LogMenager (scope) {

        this.name = 'Default';

        this.$get = function() {
            var name = this.name;
            return {
                sayHello: function() {
                    return "Hello, " + name + "!"
                }
            }
        };

        this.setName = function(name) {
            this.name = name;
        };


    }

    function config(log){
        log.flag = 'inside config';

    };

    angular.module('app.service')
        .provider('LogMenager',['$scope',LogMenager])


}());
